<?php

namespace Kowal\Banner\Block\Adminhtml\Banner\Edit\Tab;

/**
 * Banner edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Kowal\Banner\Model\Status
     */
    protected $_status;

    /**
     * Main constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Kowal\Banner\Model\Config\Source\Status $status
     * @param \Kowal\Banner\Model\Config\Source\Video $video
     * @param \Kowal\Banner\Model\Config\Source\Grupa $grupa
     * @param \Kowal\Banner\Model\Config\Source\Font $font
     * @param \Kowal\Banner\Model\Config\Source\Mobile $mobile
     * @param \Magento\Config\Model\Config\Source\Store $stores
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Kowal\Banner\Model\Config\Source\Status $status,
        \Kowal\Banner\Model\Config\Source\Video $video,
        \Kowal\Banner\Model\Config\Source\Grupa $grupa,
        \Kowal\Banner\Model\Config\Source\Font $font,
        \Kowal\Banner\Model\Config\Source\Mobile $mobile,
        \Magento\Config\Model\Config\Source\Store $stores,
        array $data = []
    )
    {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        $this->grupa = $grupa;
        $this->video = $video;
        $this->font = $font;
        $this->mobile = $mobile;
        $this->stores = $stores;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Kowal\Banner\Model\Banner */
        $model = $this->_coreRegistry->registry('kowal_banner_banner');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('banner_id', 'hidden', ['name' => 'banner_id']);
        }

        $fieldset->addField(
            'website_id',
            'select',
            [
                'label' => __('Website'),
                'title' => __('Website'),
                'name' => 'website_id',
                'required' => true,
                'options' => $this->storeOptions(),
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'sort_order',
            'text',
            [
                'name' => 'sort_order',
                'label' => __('Sort Order'),
                'title' => __('Sort Order'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'label' => __('Title'),
                'title' => __('Title'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'title_font_size',
            'select',
            [
                'label' => __('Font Size'),
                'title' => __('Font Size'),
                'name' => 'title_font_size',
                'required' => false,
                'options' => $this->font->toArray(),
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'description',
            'text',
            [
                'name' => 'description',
                'label' => __('Podtytuł'),
                'title' => __('Podtytuł'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );


        $fieldset->addField(
            'group',
            'select',
            [
                'name' => 'group',
                'label' => __('group'),
                'title' => __('Group'),
                'required' => true,
                'options' => $this->grupa->toArray(),
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'link',
            'text',
            [
                'name' => 'link',
                'label' => __('Link'),
                'title' => __('Link'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'button_text',
            'text',
            [
                'name' => 'button_text',
                'label' => __('Button Text'),
                'title' => __('Button Text'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'target',
            'select',
            [
                'label' => __('Target'),
                'title' => __('Target'),
                'name' => 'target',
                'required' => false,
                'options' => $this->getTargetOptionArray(),
                'disabled' => $isElementDisabled
            ]
        );


        $fieldset->addField(
            'image',
            'image',
            [
                'name' => 'image',
                'label' => __('Image'),
                'title' => __('Image'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'alternate',
            'text',
            [
                'name' => 'alternate',
                'label' => __('Alternate'),
                'title' => __('Alternate'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => $this->_status->toArray(),
                'disabled' => $isElementDisabled
            ]
        );


        $fieldset->addField(
            'is_video',
            'select',
            [
                'label' => __('Czy Video?'),
                'title' => __('Czy Video?'),
                'name' => 'is_video',
                'required' => true,
                'options' => $this->video->toArray(),
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'is_mobile',
            'select',
            [
                'label' => __('Na telefon'),
                'title' => __('Na telefon'),
                'name' => 'is_mobile',
                'required' => true,
                'options' => $this->mobile->toArray(),
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'color_button',
            'text',
            [
                'name' => 'color_button',
                'label' => __('Kolor przycisku'),
                'title' => __('Kolor przycisku'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'color_title',
            'text',
            [
                'name' => 'color_title',
                'label' => __('Kolor tytułu'),
                'title' => __('Kolro tytułu'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    protected function storeOptions()
    {
        $result = [];
        $o = $this->stores->toOptionArray();
        foreach ($o as $store) {
            $result[$store['value']] = $store['label'];
        }
        return $result;
    }

    public function getTargetOptionArray()
    {
        return array(
            '_self' => "Self",
            '_blank' => "New Page",
        );
    }
}
