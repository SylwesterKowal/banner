<?php

namespace Kowal\Banner\Block\Adminhtml\Banner\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Kowal\Banner\Helper\Data $helperData,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Kowal\Banner\Model\Config\Source\Image $image,
        array $data = []
    )
    {
        $this->helperData = $helperData;
        $this->storeManager = $storeManager;
        $this->configImage = $image;
        parent::__construct($context, $jsonEncoder, $authSession, $data);
    }


    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('banner_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Banner Information'));
    }

    public function getDesktopImage()
    {
        try {
            if ($img = $this->helperData->getGeneralCfg("makieta_desktop", $this->storeManager->getStore()->getWebsiteId())) {
                return $this->getMediaUrl() . $this->configImage->getUploadDir() . DIRECTORY_SEPARATOR . $img;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getMobileImage()
    {
        try {
            if ($img = $this->helperData->getGeneralCfg("makieta_mobile", $this->storeManager->getStore()->getWebsiteId())) {
                return $this->getMediaUrl() . $this->configImage->getUploadDir() . DIRECTORY_SEPARATOR . $img;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getMediaUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

}
