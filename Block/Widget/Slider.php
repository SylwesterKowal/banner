<?php
declare(strict_types=1);

namespace Kowal\Banner\Block\Widget;

use Kowal\Banner\Model\ResourceModel\Banner\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Slider extends Template implements BlockInterface
{
    public function __construct(
        CollectionFactory                                $collectionFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface       $storeManager,
        \Magento\Framework\Filesystem\DirectoryList      $dir,
        array                                            $data = []
    )
    {

        $this->collectionFactory = $collectionFactory;
        $this->storeManager = $storeManager;
        $this->dir = $dir;
        parent::__construct($context, $data);
    }

    protected $_template = "widget/slider.phtml";

    public function getItems($is_mobile)
    {
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('group', ['eq' => 'main']);
        $collection->addFieldToFilter('is_active', ['eq' => 1]);
        $collection->addFieldToFilter('website_id', ['eq' => $this->storeManager->getStore()->getId()]);

        if ($is_mobile) {
            $collection->addFieldToFilter('is_mobile', ['eq' => 1]);
        } else {
            $collection->addFieldToFilter('is_mobile', ['neq' => 1]);
        }
        $collection->setOrder('sort_order', 'ASC');

        $replace = [];
        foreach ($collection as $baner) {
            $data = $baner->getData();
            $replace_ = [];
            foreach ($data as $label => $value) {
                if ($label == 'image') {
                    $replace_[$label] = '/media/' . $value;
                } else {
                    $replace_[$label] = $value;
                }
            }

            if (file_exists($this->dir->getRoot().'/pub'.$replace_['image'])) {
                list($width, $height, $type, $attr) = getimagesize($this->dir->getRoot().'/pub'.$replace_['image']);
            } else {
                $width = null;
                $height = null;
            }
            $replace_['width'] = $width;
            $replace_['height'] = $height;
            $replace[] = $replace_;


        }
//        var_dump($replace);
        return $replace;
    }

}

