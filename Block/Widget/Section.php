<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Banner\Block\Widget;

use Kowal\Banner\Model\ResourceModel\Banner\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Section extends Template implements BlockInterface
{
//    protected $layoutFactory;

    public function __construct(

        CollectionFactory $collectionFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem\DirectoryList      $dir,
        array $data = []
    )
    {

        $this->collectionFactory = $collectionFactory;
        $this->storeManager = $storeManager;
        $this->dir = $dir;
        parent::__construct($context, $data);
    }


    protected $_template = "widget/section.phtml";

    public function getContentBlock($block_id, $is_mobile = false)
    {
        $block = (string)$this->getLayout()
            ->createBlock('Magento\Cms\Block\Block')
            ->setBlockId($block_id)
            ->toHtml();
        $pattern = "/{[^}]*}/";
        preg_match_all($pattern, $block, $fields);


//        print_r($this->getGroups($fields), true);
        return $this->getHtml($fields, $block, $is_mobile);

    }
    public function getContentBlockTemplate($is_mobile = false, $theme = null)
    {

        $block = (string)$this->getLayout()
            ->createBlock("Kowal\Banner\Block\Items")
            ->setTemplate("Kowal_Banner::themes/".$theme.".phtml")->toHtml();

        $pattern = "/{[^}]*}/";
        preg_match_all($pattern, $block, $fields);


//        print_r($this->getGroups($fields), true);
        return $this->getHtml($fields, $block, $is_mobile);

    }

    public function getItems($byGroup = 'H2')
    {
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('group', ['eq' => $byGroup]);
        $collection->addFieldToFilter('is_active', ['eq' => 1]);
        $collection->addFieldToFilter('website_id', ['eq' => $this->storeManager->getStore()->getId()]);

        $collection->setOrder('sort_order', 'ASC');

        $replace = [];
        foreach ($collection as $baner) {
            $data = $baner->getData();
            $replace_ = [];
            foreach ($data as $label => $value) {
                if ($label == 'image') {
                    $replace_[$label] = '/media/' . $value;
                } else {
                    $replace_[$label] = $value;
                }
            }

            if (file_exists($this->dir->getRoot().'/pub'.$replace_['image'])) {
                list($width, $height, $type, $attr) = getimagesize($this->dir->getRoot().'/pub'.$replace_['image']);
                $replace_['width'] = $width;
                $replace_['height'] = $height;
            }


            $replace[] = $replace_;
        }
//        var_dump($replace);
        return $replace;
    }

    public function getHtml($fields, $block, $is_mobile)
    {
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('group', ['in' => $this->getGroups($fields)]);
        $collection->addFieldToFilter('is_active', ['eq' => 1]);
        $collection->addFieldToFilter('website_id', ['eq' => $this->storeManager->getStore()->getId()]);

        if ($is_mobile){
            $collection->addFieldToFilter('is_mobile', ['eq' => 1]);
        }else{
            $collection->addFieldToFilter('is_mobile', ['neq' => 1]);
        }
        $collection->setOrder('sort_order', 'ASC');

        $paterns = [];
        $replace = [];
        foreach ($collection as $baner) {
            $data = $baner->getData();
            foreach ($data as $label => $value) {
                $paterns[] = '/{' . $baner->getGroup() . ".{$label}}/";
                if ($label == 'image') {
                    $replace[] = '/media/' . $value;
                    if (file_exists($this->dir->getRoot().'/pub/media/' . $value)) {
                        list($width, $height, $type, $attr) = getimagesize($this->dir->getRoot().'/pub/media/' . $value);
                    } else {
                        $width = null;
                        $height = null;
                    }
                } else {
                    $replace[] = $value;
                }
            }

            $paterns[] = '/{' . $baner->getGroup() . ".width}/";
            $paterns[] = '/{' . $baner->getGroup() . ".height}/";

            $replace[] = $width;
            $replace[] = $height;
        }
//        var_dump($paterns);
//        var_dump($replace);




        $block = preg_replace($paterns, $replace, $block);
        return $block;
    }


    protected function getGroups($fields)
    {
        if (isset($fields[0]) && !is_array($fields[0])) return false;
        $groups = [];
        foreach ($fields[0] as $key => $field) {
            $group = explode(".", substr($field, 1, -1))[0];
            $groups[$group] = $group;
        }
        return $groups;
    }

}

