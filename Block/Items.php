<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Banner\Block;

use Kowal\Banner\Model\ResourceModel\Banner\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Items extends Template implements BlockInterface
{
//    protected $layoutFactory;

    public function __construct(

        CollectionFactory $collectionFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {

        $this->collectionFactory = $collectionFactory;
        $this->storeManager = $storeManager;
        parent::__construct($context, $data);
    }


    public function getItems($group = 'H2')
    {
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('group', ['eq' => $group]);
        $collection->addFieldToFilter('is_active', ['eq' => 1]);
        $collection->addFieldToFilter('website_id', ['eq' => $this->storeManager->getStore()->getId()]);

        $collection->setOrder('sort_order', 'ASC');

        $replace = [];
        foreach ($collection as $baner) {
            $data = $baner->getData();
            $replace_ = [];
            foreach ($data as $label => $value) {
                if ($label == 'image') {
                    $replace_[$label] = '/media/' . $value;
                } else {
                    $replace_[$label] = $value;
                }
            }
            $replace[] = $replace_;
        }
//        var_dump($replace);
        return $replace;
    }



}

