<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Banner\Api\Data;

interface BannerInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CREATED_AT = 'created_at';
    const LINK = 'link';
    const GROUP = 'group';
    const TITLE = 'title';
    const COLOR_TITLE = 'color_title';
    const TARGET = 'target';
    const IS_VIDEO = 'is_video';
    const TITLE_FONT_SIZE = 'title_font_size';
    const IMAGE = 'image';
    const WEBSITE_ID = 'website_id';
    const BANNER_ID = 'banner_id';
    const IS_ACTIVE = 'is_active';
    const IS_MOBILE = 'is_mobile';
    const ORDER = 'sort_order';
    const ALTERNATE = 'alternate';
    const DESCRIPTION = 'description';
    const BUTTON_TEXT = 'button_text';
    const COLOR_BUTTON = 'color_button';

    /**
     * Get banner_id
     * @return string|null
     */
    public function getBannerId();

    /**
     * Set banner_id
     * @param string $bannerId
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setBannerId($bannerId);

    /**
     * Get title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     * @param string $title
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setTitle($title);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\Banner\Api\Data\BannerExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kowal\Banner\Api\Data\BannerExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\Banner\Api\Data\BannerExtensionInterface $extensionAttributes
    );

    /**
     * Get alternate
     * @return string|null
     */
    public function getAlternate();

    /**
     * Set alternate
     * @param string $alternate
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setAlternate($alternate);

    /**
     * Get description
     * @return string|null
     */
    public function getDescription();

    /**
     * Set description
     * @param string $description
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setDescription($description);

    /**
     * Get image
     * @return string|null
     */
    public function getImage();

    /**
     * Set image
     * @param string $image
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setImage($image);

    /**
     * Get link
     * @return string|null
     */
    public function getLink();

    /**
     * Set link
     * @param string $link
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setLink($link);

    /**
     * Get target
     * @return string|null
     */
    public function getTarget();

    /**
     * Set target
     * @param string $target
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setTarget($target);

    /**
     * Get is_active
     * @return string|null
     */
    public function getIsActive();

    /**
     * Set is_active
     * @param string $isActive
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setIsActive($isActive);


    /**
     * Get is_mobile
     * @return string|null
     */
    public function getIsMobile();

    /**
     * Set is_mobile
     * @param string $isActive
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setIsMobile($isMobile);

    /**
     * Get is_video
     * @return string|null
     */
    public function getIsVideo();

    /**
     * Set is_video
     * @param string $isVideo
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setIsVideo($isVideo);

    /**
     * Get group
     * @return string|null
     */
    public function getGroup();

    /**
     * Set group
     * @param string $group
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setGroup($group);

    /**
     * Get button_text
     * @return string|null
     */
    public function getButtonText();

    /**
     * Set button_text
     * @param string $buttonText
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setButtonText($buttonText);

    /**
     * Get title_font_size
     * @return string|null
     */
    public function getTitleFontSize();

    /**
     * Set title_font_size
     * @param string $titleFontSize
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setTitleFontSize($titleFontSize);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get order
     * @return string|null
     */
    public function getSortOrder();

    /**
     * Set order
     * @param string $sort_order
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setSortOrder($sort_order);

    /**
     * Get website_id
     * @return string|null
     */
    public function getWebsiteId();

    /**
     * Set website_id
     * @param string $websiteId
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setWebsiteId($websiteId);

    /**
     * Get color_title
     * @return string|null
     */
    public function getColorTitle();
    /**
     * Set color_title
     * @param string $colorTitle
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setColorTitle($colorTitle);

    /**
     * Set color_button
     * @param string $colorButton
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setColorButton($colorButton);

    /**
     * Get color_button
     * @return string|null
     */
    public function getColorButton();
}

