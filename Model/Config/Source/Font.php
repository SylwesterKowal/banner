<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Banner\Model\Config\Source;

class Font implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        $fontzise = [];
        for ($i = 10; $i <= 200; $i++) {
            $fontzise[] = ['value' => (string)$i, 'label' => (string)$i];
        }
        return $fontzise;
    }

    public function toArray()
    {
        $arr = [];
        foreach ($this->toOptionArray() as $font) {
            $arr[$font['value']] = $font['label'];
        }
        return $arr;

    }
}
