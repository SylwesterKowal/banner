<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Banner\Model\Config\Source;

class Video implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => '0', 'label' => __('No')],['value' => '1', 'label' => __('Yes')]];
    }

    public function toArray()
    {
        return ['0' => __('No'),'1' => __('Yes')];
    }
}
