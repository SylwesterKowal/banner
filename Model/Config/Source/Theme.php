<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Banner\Model\Config\Source;

class Theme implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => 'Szablon_1', 'label' => __('Szablon 1 Gr. A')],
            ['value' => 'Szablon_2', 'label' => __('Szablon_2 Gr. B')],
            ['value' => 'Szablon_3', 'label' => __('Szablon 3 Gr. C')],
            ['value' => 'Szablon_4', 'label' => __('Szablon 4 Gr. D')],
            ['value' => 'Szablon_5', 'label' => __('Szablon 5 Gr. E')],
            ['value' => 'Szablon_6', 'label' => __('Szablon 6 Gr. F')],
            ['value' => 'Szablon_7', 'label' => __('Szablon 7 Gr. G')],
            ['value' => 'Szablon_8', 'label' => __('Szablon 8 Gr. 1 & Slider')]
        ];
    }

    public function toArray()
    {
        $array = [];
        $arr = $this->toOptionArray();
        foreach ($arr as $r){
            $array[$r['value']] = $r['label'];
        }
        return $array;
    }
}