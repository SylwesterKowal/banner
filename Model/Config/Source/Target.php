<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Banner\Model\Config\Source;

class Target implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => '_blank', 'label' => __('_blank')],['value' => '_self', 'label' => __('_self')]];
    }

    public function toArray()
    {
        return ['_blank' => __('_blank'),'_self' => __('_self')];
    }
}
