<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Banner\Model\Config\Source;

class Status implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => '1', 'label' => __('Active')],['value' => '0', 'label' => __('Deactive')]];
    }

    public function toArray()
    {
        return ['1' => __('Active'),'0' => __('Deactive')];
    }
}
