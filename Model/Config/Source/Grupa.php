<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Banner\Model\Config\Source;

class Grupa implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => 'main', 'label' => __('Baner Głowny')],
            ['value' => 'A1', 'label' => __('A1')],
            ['value' => 'A2', 'label' => __('A2')],
            ['value' => 'A3', 'label' => __('A3')],
            ['value' => 'A4', 'label' => __('A4')],
            ['value' => 'A5', 'label' => __('A5')],
            ['value' => 'A6', 'label' => __('A6')],
            ['value' => 'B1', 'label' => __('B1')],
            ['value' => 'B2', 'label' => __('B2')],
            ['value' => 'B3', 'label' => __('B3')],
            ['value' => 'B4', 'label' => __('B4')],
            ['value' => 'B5', 'label' => __('B5')],
            ['value' => 'B6', 'label' => __('B6')],
            ['value' => 'C1', 'label' => __('C1')],
            ['value' => 'C2', 'label' => __('C2')],
            ['value' => 'C3', 'label' => __('C3')],
            ['value' => 'C4', 'label' => __('C4')],
            ['value' => 'C5', 'label' => __('C5')],
            ['value' => 'C6', 'label' => __('C6')],
            ['value' => 'D1', 'label' => __('D1')],
            ['value' => 'D2', 'label' => __('D2')],
            ['value' => 'D3', 'label' => __('D3')],
            ['value' => 'D4', 'label' => __('D4')],
            ['value' => 'D5', 'label' => __('D5')],
            ['value' => 'D6', 'label' => __('D6')],
            ['value' => 'E1', 'label' => __('E1')],
            ['value' => 'E2', 'label' => __('E2')],
            ['value' => 'E3', 'label' => __('E3')],
            ['value' => 'E4', 'label' => __('E4')],
            ['value' => 'E5', 'label' => __('E5')],
            ['value' => 'E6', 'label' => __('E6')],
            ['value' => 'F1', 'label' => __('F1')],
            ['value' => 'F2', 'label' => __('F2')],
            ['value' => 'F3', 'label' => __('F3')],
            ['value' => 'F4', 'label' => __('F4')],
            ['value' => 'F5', 'label' => __('F5')],
            ['value' => 'F6', 'label' => __('F6')],
            ['value' => 'G1', 'label' => __('G1')],
            ['value' => 'H1', 'label' => __('H1')],
            ['value' => 'H2', 'label' => __('H2')],
            ['value' => 'H3', 'label' => __('H3')],
            ['value' => 'H4', 'label' => __('H4')],
            ['value' => 'H5', 'label' => __('H5')],
            ['value' => 'H6', 'label' => __('H6')]
        ];
    }

    public function toArray()
    {
        $array = [];
        $arr = $this->toOptionArray();
        foreach ($arr as $r){
            $array[$r['value']] = $r['label'];
        }
        return $array;
    }
}
