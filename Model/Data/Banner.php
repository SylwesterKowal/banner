<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Banner\Model\Data;

use Kowal\Banner\Api\Data\BannerInterface;

abstract class Banner extends \Magento\Framework\Api\AbstractExtensibleObject implements BannerInterface
{

    /**
     * Get banner_id
     * @return string|null
     */
    public function getBannerId()
    {
        return $this->_get(self::BANNER_ID);
    }

    /**
     * Set banner_id
     * @param string $bannerId
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setBannerId($bannerId)
    {
        return $this->setData(self::BANNER_ID, $bannerId);
    }

    /**
     * Get title
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * Set title
     * @param string $title
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\Banner\Api\Data\BannerExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Kowal\Banner\Api\Data\BannerExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\Banner\Api\Data\BannerExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get alternate
     * @return string|null
     */
    public function getAlternate()
    {
        return $this->_get(self::ALTERNATE);
    }

    /**
     * Set alternate
     * @param string $alternate
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setAlternate($alternate)
    {
        return $this->setData(self::ALTERNATE, $alternate);
    }

    /**
     * Get description
     * @return string|null
     */
    public function getDescription()
    {
        return $this->_get(self::DESCRIPTION);
    }

    /**
     * Set description
     * @param string $description
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Get image
     * @return string|null
     */
    public function getImage()
    {
        return $this->_get(self::IMAGE);
    }

    /**
     * Set image
     * @param string $image
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * Get link
     * @return string|null
     */
    public function getLink()
    {
        return $this->_get(self::LINK);
    }

    /**
     * Set link
     * @param string $link
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setLink($link)
    {
        return $this->setData(self::LINK, $link);
    }

    /**
     * Get target
     * @return string|null
     */
    public function getTarget()
    {
        return $this->_get(self::TARGET);
    }

    /**
     * Set target
     * @param string $target
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setTarget($target)
    {
        return $this->setData(self::TARGET, $target);
    }

    /**
     * Get is_active
     * @return string|null
     */
    public function getIsActive()
    {
        return $this->_get(self::IS_ACTIVE);
    }

    /**
     * Set is_active
     * @param string $isActive
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * Get is_mobile
     * @return string|null
     */
    public function getIsMobile()
    {
        return $this->_get(self::IS_MOBILE);
    }

    /**
     * Set is_mobile
     * @param string $isMobile
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setIsMobile($isMobile)
    {
        return $this->setData(self::IS_MOBILE, $isMobile);
    }

    /**
     * Get is_video
     * @return string|null
     */
    public function getIsVideo()
    {
        return $this->_get(self::IS_VIDEO);
    }

    /**
     * Set is_video
     * @param string $isVideo
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setIsVideo($isVideo)
    {
        return $this->setData(self::IS_VIDEO, $isVideo);
    }

    /**
     * Get group
     * @return string|null
     */
    public function getGroup()
    {
        return $this->_get(self::GROUP);
    }

    /**
     * Set group
     * @param string $group
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setGroup($group)
    {
        return $this->setData(self::GROUP, $group);
    }

    /**
     * Get button_text
     * @return string|null
     */
    public function getButtonText()
    {
        return $this->_get(self::BUTTON_TEXT);
    }

    /**
     * Set button_text
     * @param string $buttonText
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setButtonText($buttonText)
    {
        return $this->setData(self::BUTTON_TEXT, $buttonText);
    }

    /**
     * Get title_font_size
     * @return string|null
     */
    public function getTitleFontSize()
    {
        return $this->_get(self::TITLE_FONT_SIZE);
    }

    /**
     * Set title_font_size
     * @param string $titleFontSize
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setTitleFontSize($titleFontSize)
    {
        return $this->setData(self::TITLE_FONT_SIZE, $titleFontSize);
    }

    /**
     * Get color_title
     * @return string|null
     */
    public function getColorTitle()
    {
        return $this->_get(self::COLOR_TITLE);
    }

    /**
     * Set color_title
     * @param string $colorTitle
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setColorTitle($colorTitle)
    {
        return $this->setData(self::COLOR_TITLE, $colorTitle);
    }
    /**
     * Get color_button
     * @return string|null
     */
    public function getColorButton()
    {
        return $this->_get(self::COLOR_BUTTON);
    }

    /**
     * Set color_button
     * @param string $colorButton
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setColorButton($colorButton)
    {
        return $this->setData(self::COLOR_BUTTON, $colorButton);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get order
     * @return string|null
     */
    public function getSortOrder()
    {
        return $this->_get(self::ORDER);
    }

    /**
     * Set order
     * @param string $order
     * @return \Kowal\Banner\Api\Data\BannerInterface
     */
    public function setSortOrder($order)
    {
        return $this->setData(self::ORDER, $order);
    }
}

