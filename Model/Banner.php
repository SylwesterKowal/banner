<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Banner\Model;

use Kowal\Banner\Api\Data\BannerInterface;
use Kowal\Banner\Api\Data\BannerInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Banner extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'kowal_banner_banner';
    protected $dataObjectHelper;

    protected $bannerDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param BannerInterfaceFactory $bannerDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Kowal\Banner\Model\ResourceModel\Banner $resource
     * @param \Kowal\Banner\Model\ResourceModel\Banner\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        BannerInterfaceFactory $bannerDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Kowal\Banner\Model\ResourceModel\Banner $resource,
        \Kowal\Banner\Model\ResourceModel\Banner\Collection $resourceCollection,
        array $data = []
    ) {
        $this->bannerDataFactory = $bannerDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve banner model with banner data
     * @return BannerInterface
     */
    public function getDataModel()
    {
        $bannerData = $this->getData();
        
        $bannerDataObject = $this->bannerDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $bannerDataObject,
            $bannerData,
            BannerInterface::class
        );
        
        return $bannerDataObject;
    }
}

