<?php
/**
 * hQuery Scraper deltaww.com
 * Copyright (C) 2019  kowal sp. z o.o.
 *
 * This file included in Kowal/ScraperHQuery is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace Kowal\ScraperHQuery\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_kowal_scraperhquery_banner = $setup->getConnection()->newTable($setup->getTable('kowal_scraperhquery_banner'));

        $table_kowal_scraperhquery_banner->addColumn(
            'banner_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => False,'identity' => true],
            'title'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'alternate',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'alternate'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'description'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'image',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => False],
            'image'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'link',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'link'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'target',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'target'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            null,
            [],
            'is_active'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'is_video',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            null,
            [],
            'is_video'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'is_mobile',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            null,
            [],
            'is_mobile'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'group',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'group'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'button_text',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'button_text'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'title_font_size',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'title_font_size'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'created_at'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'sort_order',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'sort_order'
        );

        $table_kowal_scraperhquery_banner->addColumn(
            'website_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'website_id'
        );

        $setup->getConnection()->createTable($table_kowal_scraperhquery_banner);
    }
}
